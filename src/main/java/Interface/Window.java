package Interface;

import Movie.MovieType;
import Movie.Moviee;
import com.toedter.calendar.JSpinnerDateEditor;
import lombok.Getter;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

@Getter
public class Window {


    private JPanel panel1;
    private JTextField tytulField;
    private JRadioButton takRadioButton;
    private JTextField rezysertextField;
    private JComboBox gatunekcomboBox;
    private JSlider ocenaslider1;
    private JButton dodajFilmButton;
    private JTable table1;
    private JSpinnerDateEditor daterSpinner;
    private JLabel NowyFilm;
    private JButton zapiszPlikButton;
    private JProgressBar zapiszprogressBar2;
    private JButton usuńFilmButton;
    private JRadioButton nieRadioButton;
    private JButton zaladujButton;
    private JProgressBar wczytajprogressBar1;
    public JComboBox comboBox1;
    public JButton uaktualnijFilmButton;
    public JButton wyczyśćPolaButton;
    private MovieTableModel model;
    private int selectedIndexes;
    private File file = new File("/Users/marekdybusc/IdeaProjects/Programowanie2Amen/MovieDatabase/src/main/java/MovieData.txt");

    public Window() {
        this.model = MovieTableModel.INSTANCE;
        table1.setModel(model);
        table1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        dodajFilmButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Moviee m = new Moviee(tytulField.getText(), Movie.MovieType.valueOf(gatunekcomboBox.getSelectedItem().toString()), comboBox1.getSelectedItem().toString(), rezysertextField.getText(), takRadioButton.isSelected(), ocenaslider1.getValue());
                model.addMovie(m);
                System.out.println(model.movieSet);
                model.fireTableDataChanged();
                table1.repaint();
            }
        });

        ListSelectionModel selectionModel = table1.getSelectionModel();
        selectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (table1.getSelectedRowCount() == 0) {
                    usuńFilmButton.setEnabled(false);
                }
                else{
                    getNowyFilm().setText("Edytowany Film");
                    UUID uuid = (UUID) table1.getValueAt(table1.getSelectedRow(), 6);
                    Moviee tmp = model.movieMap.get(uuid);
                    usuńFilmButton.setEnabled(true);
                    System.out.println(model.getMovieMap().get(uuid));
                    tytulField.setText(model.getMovieMap().get(uuid).getMovieName());
                    for (int i = 0; i < gatunekcomboBox.getItemCount(); i++) {
                        if(gatunekcomboBox.getItemAt(i).toString().equals(String.valueOf(tmp.getMovieType()))){
                            gatunekcomboBox.setSelectedIndex(i);
                        }
                    }

                    for (int i = 0; i < comboBox1.getItemCount(); i++) {
                        if(comboBox1.getItemAt(i).toString().equals(String.valueOf(tmp.getReleaseDate()))){
                            comboBox1.setSelectedIndex(i);
                        }
                    }
                    rezysertextField.setText(String.valueOf(tmp.getDirectorName()));
                    ocenaslider1.setValue(tmp.getRating());
                    takRadioButton.setSelected(tmp.isWatched());
                    nieRadioButton.setSelected(!tmp.isWatched());

                }
            }
        });

        usuńFilmButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeMovies(model.movieSet.get(selectedIndexes));
            }
        });


        zapiszPlikButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                zapiszPlikButton.setEnabled(false);
                zapiszPlikButton.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                WriteWithBar task = new WriteWithBar();
                task.done = false;
                task.addPropertyChangeListener(new PropertyChangeListener() {
                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {
                        if("progress" == evt.getPropertyName()){
                            int progress = (Integer) evt.getNewValue();
                            zapiszprogressBar2.setValue(progress);
                        }
                    }
                });
                task.execute();
            }
        });


        zaladujButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ReadWithBar task = new ReadWithBar();
                task.done = false;
                task.addPropertyChangeListener(new PropertyChangeListener() {
                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {
                        if("progress" == evt.getPropertyName()){
                            int progress = (Integer) evt.getNewValue();
                            wczytajprogressBar1.setValue(progress);
                        }
                    }
                });
                task.execute();
            }
        });
        wyczyśćPolaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearFields();
                getNowyFilm().setText("Nowy Film");
            }
        });
        uaktualnijFilmButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Moviee m = new Moviee(tytulField.getText(), Movie.MovieType.valueOf(gatunekcomboBox.getSelectedItem().toString()), comboBox1.getSelectedItem().toString(), rezysertextField.getText(), takRadioButton.isSelected(), ocenaslider1.getValue());
                model.removeMovies(model.movieMap.get(table1.getValueAt(table1.getSelectedRow(), 6)));
                model.addMovie(m);
                model.fireTableDataChanged();
            }
        });
    }

    class WriteWithBar extends SwingWorker<Void, Void> {
        boolean done;

        @Override
        protected void done() {
            zapiszPlikButton.setEnabled(true);
            zapiszPlikButton.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            zapiszprogressBar2.setValue(zapiszprogressBar2.getMinimum());
        }

        @Override
        protected Void doInBackground() throws Exception {
            setProgress(0);
            for (int i = 0; i < 101; i++) {
                Thread.sleep(10);
                setProgress(i);
            }
            try (PrintWriter pw = new PrintWriter(new FileWriter(file, false))) {
                for (Moviee m : model.movieSet) {
                    pw.println(m);
                    pw.println("-separator-");
                }
                    pw.flush();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            return null;
        }
    }

    class ReadWithBar extends SwingWorker<Void, Void> {
        boolean done;

        @Override
        protected void done() {
            zaladujButton.setEnabled(true);
            zaladujButton.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            wczytajprogressBar1.setValue(wczytajprogressBar1.getMinimum());
        }

        @Override
        protected Void doInBackground() throws Exception {
            zaladujButton.setEnabled(false);
            zaladujButton.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            model.clear();
            model.fireTableDataChanged();
            setProgress(0);
            for (int i = 0; i < 101; i++) {
                Thread.sleep(6);
                setProgress(i);
            }
            try (Scanner scan = new Scanner(file)) {
                Moviee m = new Moviee("", MovieType.HORROR, "2001", "", true, 1);
                while (scan.hasNextLine()){
                    String line = scan.nextLine();
                    String[] lineArguments = line.split(":", 2);
                    String keyword = lineArguments[0];

                    if (keyword.contains("MovieName")) {
                        m = new Moviee();
                        m.setMovieName(lineArguments[1].trim());
                    } else if (keyword.contains("MovieType")) {
                        m.setMovieType(MovieType.valueOf(lineArguments[1].trim()));
                    } else if (keyword.contains("Year")) {
                        m.setReleaseDate(lineArguments[1].trim());
                    } else if (keyword.contains("DirectorName")) {
                        m.setDirectorName(lineArguments[1].trim());
                    } else if (keyword.contains("Watched")){
                        m.setWatched(Boolean.parseBoolean(lineArguments[1].trim()));
                    } else if (keyword.contains("Rating")){
                        m.setRating(Integer.parseInt(lineArguments[1].trim()));
                    } else if (keyword.contains("separator")) {
                        m.setId(UUID.randomUUID());
                        model.addMovie(m);
                    }
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            model.fireTableDataChanged();
            return null;
        }
    }

    public void clearFields(){
        tytulField.setText("");
        gatunekcomboBox.setSelectedIndex(1);
        comboBox1.setSelectedIndex(108);
        rezysertextField.setText("");
        ocenaslider1.setValue(1);
        takRadioButton.setSelected(false);
        nieRadioButton.setSelected(false);
    }

}

