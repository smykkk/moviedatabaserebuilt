package Movie;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter

public class Moviee {
    private UUID id;
    private String movieName;
    private MovieType movieType;
    private String releaseDate;
    private String directorName;
    private Integer rating;
    private boolean isWatched;

    public Moviee(String movieName, MovieType movieType, String releaseDate, String directorName, boolean isWatched, Integer rating) {
        this.movieName = movieName;
        this.movieType = movieType;
        this.releaseDate = releaseDate;
        this.directorName = directorName;
        this.rating = rating;
        this.isWatched = isWatched;
        this.id = UUID.randomUUID();
    }

    public Moviee() {
    }

    @Override
    public String toString() {
            return "MovieName: " + movieName + "\nMovieType: " + movieType + "\nYear: " + releaseDate + "\nDirectorName: " + directorName + "\nRating: " + rating
                    +"\nWatched: " + isWatched;
    }
}
