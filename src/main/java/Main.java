import Interface.WindowFrame;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        WindowFrame frame = new WindowFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
